db.fruits.insertMany([
    {
        name: "Apple",
        color: "Red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Philippines","US"]
    },
    {
        name: "Banana",
        color: "Yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Philippines","Ecuador"]
    },
    {
        name: "Kiwi",
        color: "Green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US","China"]
    },
    {
        name: "Mango",
        color: "Yellow",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Philippines","India"]
    }
])

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group: {_id:"$onSale",count: {$sum:1}}}
])

db.fruits.aggregate([
    {$match:{stock:{$gt:20}}},
    {$group: {_id:"$stock",count: {$sum:1}}}
])

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier_id",totalAverage:{$avg:"$price"}}}
])

db.fruits.aggregate([
    {$group:{_id:"$supplier_id",highestPrice:{$max:"$price"}}}
])
db.fruits.aggregate([
    {$group:{_id:"$supplier_id",lowestPrice:{$min:"$price"}}}
])